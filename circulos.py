import random
from stddraw import Picture

if __name__ == '__main__':
    print 'Ingrese #Circulos, Prob(Negros), RMin, RMax '
    print 'Ejem: 10 0.4 0.2 0.4'
    raw = raw_input('==> ')

    try:
        num_circulos, prob, rmin, rmax = raw.split(' ')
        num_circulos = int(num_circulos)
        prob = float(prob)
        rmin = float(rmin)
        rmax = float(rmax)

    except Exception:
        print 'Mal ingreso'
        print 'Ejem: 10 0.4 0.2 0.4'
        exit(0)
    else:
        pass

    picture = Picture()

    color_negro = picture.makeColor(255, 255, 255)
    color_blanco = picture.makeColor(0, 0, 0)

    num_negros = int(num_circulos * prob)

    colores = []

    for x in range(num_circulos):
        if x < num_negros:
            colores.append(color_negro)
        else:
            colores.append(color_blanco)

    random.shuffle(colores)

    for color in colores:
        radio = random.randrange(rmin * 100.0, rmax * 100, 1) / 100.0
        x = random.randrange(0, 100, 1) / 100.0
        y = random.randrange(0, 100, 1) / 100.0

        picture.filledCircle(x, y, radio, color)
    picture.start()
