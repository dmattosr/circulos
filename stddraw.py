#!/usr/bin/python
# Filename: stddraw.py
# File: stddraw.py
# Author: Pine Wu, with help from Robert Muller
# Description: This is a python drawing package that is used for simple drawing, with mouse interection provided.
#              It is an imitation of Robert Sedgewick's StdDraw package in Java. URL: http://algs4.cs.princeton.edu/code/

# Update 1:
# Date: Jan, 13th, 2013.
# Scale, as a Scrollbar is added.
# Scale methods are added to Panel and Picture classes.

# This is a bogus comment.

# IMPORTANT:
# If using python 2.7, change the "tkinter" in the following line to "Tkinter".
from Tkinter import *

from random import *

print 'stddraw v0.2'

# A simple test making sure the program works properly.
TEST = False
PANEL_TEST = False

##################################################
##################################################
##################################################
# Actual implementation of drawing. Creates a tkinter Canvas object as the canvas.
# Acronyms: w = width, h = height, r = radius

class Draw(object):
    def __init__(self, master):
        self.canvas = Canvas(master.root, width=512, height=512, bg="White")
        self.canvas.grid(row=0, column=0)

        self.main = master
        self.master = master.root

        self.xScale = 1
        self.yScale = 1

    def getW(self):
        return int(self.canvas.cget("width")) / self.xScale
    def getH(self):
        return int(self.canvas.cget("height")) / self.yScale

    def setW(self, w):
        self.canvas.config(width=w)
    def setH(self, h):
        self.canvas.config(height=h)

    def setXScale(self, s):
        self.xScale = s
    def setYScale(self, s):
        self.yScale = s
        
    def clear(self):
        self.canvas.delete(*self.canvas.find_all())

#w = canvas width; width = line width
    def line(self, x0, y0, x1, y1, color, width):
        w, h = self.getW(), self.getH()
        x0Pixel, x1Pixel = x0 * w, x1 * w
        y0Pixel, y1Pixel = (1 - y0) * h, (1 - y1) * h
        
        return self.canvas.create_line(x0Pixel, y0Pixel, x1Pixel, y1Pixel, fill=color, width=width)

    def arc(self, x0, y0, halfWidth, halfHeight, startAngle, degree, color):
        w, h = self.getW(), self.getH()
        x0Pixel, x1Pixel = (x0 - halfWidth) * w, (x0 + halfWidth) * w
        y0Pixel, y1Pixel = (1 - (y0 - halfHeight)) * h, (1 - (y0 + halfHeight)) * h
        startAngle += 90
        degree *= -1

        return self.canvas.create_arc(x0Pixel, y0Pixel, x1Pixel, y1Pixel, start=startAngle, extent=degree, fill=color)
    
    def oval(self, x0, y0, halfWidth, halfHeight, color):
        w, h = self.getW(), self.getH()
        x0Pixel, x1Pixel = (x0 - halfWidth) * w, (x0 + halfWidth) * w
        y0Pixel, y1Pixel = (1 - (y0 - halfHeight)) * h, (1 - (y0 + halfHeight)) * h

        return self.canvas.create_oval(x0Pixel, y0Pixel, x1Pixel, y1Pixel, fill=color)        

    def square(self, x0, y0, r, color):
        return self.rectangle(x0, y0, r, r, color)

    def rectangle(self, x0, y0, halfWidth, halfHeight, color):
        w, h = self.getW(), self.getH()
        x0Pixel, x1Pixel = (x0 - halfWidth) * w, (x0 + halfWidth) * w
        y0Pixel, y1Pixel = (1 - (y0 - halfHeight)) * h, (1 - (y0 + halfHeight)) * h

        return self.canvas.create_rectangle(x0Pixel, y0Pixel, x1Pixel, y1Pixel, fill=color)

    def polygon(self, xList, yList):
        w, h = self.getW(), self.getH()

        for i in range(len(xList)):
            tempLine = self.line(xList[i], yList[i], xList[(i + 1) % len(xList)], yList[(i + 1) % len(xList)], "Black", 1)
            self.canvas.itemconfig(tempLine, tags="polygon")

        return "polygon"
    
    def filledPolygon(self, xList, yList, color):
        w, h = self.getW(), self.getH()
        
        args = []
        for i in range(len(xList)):
            args.append(xList[i] * w)
            args.append((1 - yList[i]) * h)

        return  self.canvas.create_polygon(*args, fill=color)

    def text(self, x0, y0, message, anchor):
        w, h = self.getW(), self.getH()

        x0Pixel, y0Pixel = x0 * w, (1 - y0) * h
        return self.canvas.create_text(x0Pixel, y0Pixel, text=message)

    def image(self, x0, y0, photo, anchor):
        w, h = self.getW(), self.getH()

        x0Pixel, y0Pixel = x0 * w, (1 - y0) * h        
        return self.canvas.create_image(x0Pixel, y0Pixel, image=photo, anchor=anchor)

##################################################
    def move(self, item, x0, y0):
        w, h = self.getW(), self.getH()
        x0Pixel = x0 * w
        y0Pixel = -1 * y0 * h
        
        self.canvas.move(item, x0Pixel, y0Pixel)
    
    def delete(self, item):
        self.canvas.delete(item)

    def configColor(self, item, color):
        self.canvas.itemconfig(item, fill=color)

    def configText(self, item, message):
        self.canvas.itemconfig(item, text=message)

    def wait(self, ms):
        self.canvas.update()
        self.canvas.after(ms)

##################################################
##################################################
##################################################
# A panel for the use of input/interection.

class Panel(object):

    def __init__(self, master):
        self.frame = Frame(master.root, height=512, width=256)
        self.frame.grid(row=0, column=1)
        self.frame.grid_forget()

        self.main = master
        self.master = master.root
        
        self.frame.columnconfigure(0, minsize=128)
        self.frame.columnconfigure(1, minsize=128)
        for i in range(8):
            self.frame.rowconfigure(i, minsize=60)
        self.frame.rowconfigure(8, minsize=32)

        # Four buttons, b1, b2, b3, b4:
        b1 = Button(self.frame, text="b1(disabled)", state="disabled", width=15)
        b1.grid(row=0, column=0)
        b2 = Button(self.frame, text="b2(disabled)", state="disabled", width=15)
        b2.grid(row=0, column=1)
        b3 = Button(self.frame, text="b3(disabled)", state="disabled", width=15)
        b3.grid(row=1, column=0)
        b4 = Button(self.frame, text="b4(disabled)", state="disabled", width=15)
        b4.grid(row=1, column=1)
        self.b = [b1, b2, b3, b4]

        # Three entries, e1, e2, e3:
        e1 = Entry(self.frame, state="disabled", width=30)
        e1.grid(row=2, columnspan=2)
        e2 = Entry(self.frame, state="disabled", width=30)
        e2.grid(row=3, columnspan=2)
        e3 = Entry(self.frame, state="disabled", width=30)
        e3.grid(row=4, columnspan=2)
        self.e = [e1, e2, e3]

        # Two check buttons, c1, c2, with their indicators i1, i2:
        i1 = IntVar()
        c1 = Checkbutton(self.frame, text="c1(disabled)", state="disabled", width=30, variable=i1)
        c1.grid(row=5, columnspan=2)
        i2 = IntVar()
        c2 = Checkbutton(self.frame, text="c2(disabled)", state="disabled", width=30, variable=i2)
        c2.grid(row=6, columnspan=2)
        self.i = [i1, i2]
        self.c = [c1, c2]
        
        # One label l, used as a status bar:
        l = Label(self.frame, text="", width=30, relief="sunken")
        l.grid(row=7, columnspan=2)
        self.l = l

        # One scale s, used as a slider:
        s = Scale(self.frame, from_=0, to=100, orient="horizontal", state="disabled", length=200)
        s.grid(row=8, columnspan=2)
        self.s = s

##################################################
##################################################
# Panel methods:

    def showPanel(self):
        self.master.columnconfigure(1, minsize=256)
        h = self.main.Draw.getH()
        self.frame.configure(height=h)
        self.frame.grid(row=0, column=1)
    
    def hidePanel(self):                                            
        self.master.columnconfigure(1, minsize=0)
        self.frame.grid_forget()

##################################################

    def enableButton(self, number, text, command):
        self.b[number - 1].config(text=text, command=command, state="normal")

    def setButtonName(self, number, text, command):
        self.b[number - 1].config(text=text, command=command)

    def disableButton(self, number):
        self.b[number - 1].config(text="b%d(disabled)" % number, state="disabled")

##################################################

    def enableEntry(self, number):
        self.e[number - 1].config(state="normal")

    def getEntry(self, number):
        return self.e[number - 1].get()

    def emptyEntry(self, number, text):
        self.e[number - 1].delete(0, "end")

    def disableEntry(self, number):
        self.e[number - 1].delete(0, "end")
        self.e[number - 1].config(state="disabled")

##################################################
        
    def enableCheckbutton(self, number, text, command):
        self.c[number - 1].config(text=text, command=command, state="normal")

    def configCheckbutton(self, number, text, command):
        self.c[number - 1].config(text=text, command=command)

    def isChecked(self, number):
        return self.i[number - 1].get()

    def disableCheckbutton(self, number):
        self.c[number - 1].config(text="c%d(disabled)" % number, state="disabled")

##################################################
    
    def enableStatus(self, text):
        self.l.config(text=text, state="normal")
        
    def configStatus(self, text):
        self.l.config(text=text)

    def getStatus(self):
        return self.l.cget("text")
        
    def disableStatus(self):
        self.l.config(text="", state="disabled")

##################################################

    def enableSlider(self):
        self.s.config(from_=0, to=100, command=None, state="normal")

    def configSlider(self, start, end, command):
        self.s.config(from_=start, to=end, command=command)

    def getSliderValue(self):
        return self.s.get()

    def disableSlider(self):
        self.s.config(state="disabled")
            

##################################################
##################################################
##################################################

# A class that provides public methods. It creates a Tk object and an instance of Draw, whose canvas has Tk as the master.

class Picture(object):
    
    def __init__(self):        
        self.root = Tk()
        self.root.columnconfigure(1, minsize=0)
        
        self.Draw = Draw(self) 
        self.Panel = Panel(self)

##################################################   
##################################################
##### Draw methods:

#Event methods:
    # common button: Left click ->"<Button-1>"
    #                Right click -> "<Button-3>"
    #                Mouse on Canvas -> "<Enter>"
    #                Mouse leaves Canvas -> "<Leave>"
    def bind(self, button, event):
        self.Draw.canvas.bind(button, event)
        
    
##################################################
#Drawing methods
#   Returned are handlers--reference to items; they can be used to move/delete/config items.
#   All x and y are from 0 to 1 scale.

    def line(self, x0, y0, x1, y1, color="Black", penWidth=1):
        return self.Draw.line(x0, y0, x1, y1, color, penWidth)

    def arc(self, x0, y0, halfWidth, halfHeight, startAngle, degree):
        return self.Draw.arc(x0, y0, halfWidth, halfHeight, startAngle, degree, None)

    def filledArc(self, x0, y0, halfWidth, halfHeight, startAngle, degree, color="Black"):
        return self.Draw.arc(x0, y0, halfWidth, halfHeight, startAngle, degree, color)

    def oval(self, x0, y0, halfWidth, halfHeight):
        return self.Draw.oval(x0, y0, halfWidth, halfHeight, None)

    def filledOval(self, x0, y0, halfWidth, halfHeight, color="Black"):
        return self.Draw.oval(x0, y0, halfWidth, halfHeight, color)

    def circle(self, x0, y0, radius):
        return self.Draw.oval(x0, y0, radius, radius, None)

    def filledCircle(self, x0, y0, radius, color="Black"):
        return self.Draw.oval(x0, y0, radius, radius, color)
    
    def rectangle(self, x0, y0, halfWidth, halfHeight):
        return self.Draw.rectangle(x0, y0, halfWidth, halfHeight, None)

    def filledRectangle(self, x0, y0, halfWidth, halfHeight, color="Black"):
        return self.Draw.rectangle(x0, y0, halfWidth, halfHeight, color)

    def square(self, x0, y0, radius):
        return self.Draw.square(x0, y0, radius, None)

    def filledSquare(self, x0, y0, radius, color="Black"):
        return self.Draw.square(x0, y0, radius, color)

    def polygon(self, xList, yList):
        if len(xList) != len(yList):
            raise TypeError("Provide x, y coordinates in pairs.")
        return self.Draw.polygon(xList, yList)

    def filledPolygon(self, xList, yList, color="Black"):
        if len(xList) != len(yList):
            raise TypeError("Provide x, y coordinates in pairs.")
        return self.Draw.filledPolygon(xList, yList, color)

    def text(self, x0, y0, message, anchor="sw"):
        return self.Draw.text(x0, y0, message, anchor)

    def readGif(self, photoName):
        return PhotoImage(file=photoName)
    
    def image(self, x0, y0, photo, anchor="sw"):
        return self.Draw.image(x0, y0, photo, anchor)

##################################################
#Manimulating methods:
    def move(self, item, x0, y0):
        self.Draw.move(item, x0, y0)
        
    def delete(self, item):
        self.Draw.delete(item)

    def configColor(self, item, color):
        self.Draw.configColor(item, color)
        
    def configText(self, item, message):
        self.Draw.configText(item, message)

    def wait(self, milliseconds, event=None):
        self.Draw.wait(milliseconds)
        event

##################################################
#Special methods:
    def makeColor(self, r, g, b):
        return "#%02x%02x%02x" % (r, g, b)

    def randomColor(self):
        return self.makeColor(randint(0, 255), randint(0, 255), randint(0, 255))

    def setW(self, w):
        self.Draw.setW(w)
    def setH(self, h):
        self.Draw.setH(h)

    def getW(self):
        return int(self.Draw.canvas.cget("width"))
    def getH(self):
        return int(self.Draw.canvas.cget("height"))

    def setXScale(self, s):
        self.Draw.setXScale(s)
    def setYScale(self, s):
        self.Draw.setYScale(s)
        
    def clear(self):
        self.Draw.clear()

    def start(self):
        self.root.mainloop()

##################################################        
##################################################
##### Panel methods:

##################################################
#Show/hide panel:
        
    def showPanel(self):
        self.Panel.showPanel()

    def hidePanel(self):
        self.Panel.hidePanel()

##################################################
#enable/config buttons, entries, checkboxes, status
    def enableButton(self, number, text, command):
        self.Panel.enableButton(number, text, command)

    def configButton(self, number, text, command):
        self.Panel.configButton(number, text, command)

    def disableButton(self, number):
        self.Panel.disableButton(number)

##################################################

    def enableEntry(self, number):
        self.Panel.enableEntry(number)

    def getEntry(self, number):
        return self.Panel.getEntry(number)

    def emptyEntry(self, number):
        self.Panel.emptyEntry(number)

    def disableEntry(self, number):
        self.Panel.disableEntry(number)

##################################################
        
    def enableCheckbutton(self, number, text, command=lambda:None):
        self.Panel.enableCheckbutton(number, text, command)

    def configCheckbutton(self, number, text, command=lambda:None):
        self.Panel.configCheckbutton(number, text, command)

    def isChecked(self, number):
        if self.Panel.isChecked(number) == 1:
            return True
        else:
            return False

    def disableCheckbutton(self, number):
        self.Panel.disableCheckbutton(number)

##################################################

    def enableStatus(self, text=""):
        self.Panel.enableStatus(text)
        
    def configStatus(self, text):
        self.Panel.config(text)

    def getStatus(self):
        return self.Panel.getStatus()

    def disableStatus(self):
        self.Panel.disableStatus()

##################################################

    def enableSlider(self):
        self.Panel.enableSlider()

    def configSlider(self, start, end, command=lambda:None):
        self.Panel.configSlider(start, end, command)

    def getSliderValue(self):
        return self.Panel.getSliderValue()

    def disableSlider(self):
        self.Panel.disableSlider()
        
##################################################
        ##################################################

    

if TEST:
    test = Picture()
    test.setXScale(2)
    test.setYScale(3)
    
    msg = test.text(.7, .95, "Test drawing methods")
    test.wait(500)

    head = test.circle(.5, .8, .1)
    test.wait(100)

    test.oval(.5, .5, .1, .2)
    test.wait(100)

    test.line(.4, .5, .25, .65)
    test.line(.6, .5, .75, .65)
    test.wait(100)

    test.filledSquare(.5, .2, .1)

    test.polygon([.5, .3, .4], [.3, .1, .1])
    test.polygon([.5, .7, .6], [.3, .1, .1])
    test.wait(100)

    test.filledRectangle(.5, .1, .5, .05, None)
    test.wait(100)

    arc = test.filledArc(.15, .85, .05, .05, 180, 270, "Yellow")
    test.wait(500, test.configText(msg, "Finished, now trying move, config, delete, clear, setW"))

    test.wait(2000)

    color = test.randomColor()
    test.configColor(arc, color)
    test.move(arc, .0, -.3)
    test.wait(300)

    test.delete(arc)
    test.wait(300)

    test.setW(1000)
    test.wait(300)

    
    test.bind("<Button-3>", lambda e:test.clear())

    test.start()

if PANEL_TEST:
    test = Picture()
    test.showPanel()
    
    test.enableStatus()
    test.enableEntry(1)
    test.enableEntry(2)
    test.enableEntry(3)
    test.enableCheckbutton(1, "Enable button 1",
                             lambda:test.enableButton(1, "Draw a square",
                                                        lambda: test.square(float(test.getEntry(1)), float(test.getEntry(2)), float(test.getEntry(3)))))
    test.enableCheckbutton(2, "Enable button 2",
                             lambda:test.enableButton(2, "Draw a circle",
                                                        lambda: test.circle(float(test.getEntry(1)), float(test.getEntry(2)), float(test.getEntry(3)))))

#    test.enableButton(3, "Disable entry 1", lambda:test.disableEntry(1))
#    test.enableButton(4, "Disable Status", lambda:test.disableStatus())
    test.enableButton(3, "Enable slider", lambda:test.enableSlider())
    test.enableButton(4, "Config slider", lambda:test.configSlider(0, 10, lambda e:test.text(random(), random(), str(test.getSliderValue()))))


    test.enableStatus("Panel in use")    
    test.start()
    

    












    
    